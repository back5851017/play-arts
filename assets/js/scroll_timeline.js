$('.container').scroll(function(e) {
    var scrollAmount = $('.container').scrollTop();
    var documentHeight = $(document).height();
    var CountSection = $('section').length - 1;
    var scrollPercent = (scrollAmount / documentHeight / CountSection) * 50;

    $(".scrollBar").css("height", scrollPercent + "%");
    //$(".scrollBar span").text();
});