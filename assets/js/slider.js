var mouse = {
        X: 0,
        Y: 0,
        CX: 0,
        CY: 0
    },
    block = {
        X: mouse.X,
        Y: mouse.Y,
        CX: mouse.CX,
        CY: mouse.CY
    }

$('.block').on('mousemove', function(e) {
    mouse.X = (e.pageX - $(this).offset().left) - $('.block').width() / 2;
    mouse.Y = (e.pageY - $(this).offset().top) - $('.block').height() / 2;
})

$('.block').on('mouseleave', function(e) {
    mouse.X = mouse.CX;
    mouse.Y = mouse.CY;
})

setInterval(function() {

    block.CY += (mouse.Y - block.CY) / 12;
    block.CX += (mouse.X - block.CX) / 12;

}, 20);

$('.slider .item').each(function(i) {

    if (i == 0) {

        $(this).addClass('active');
        $(this).next().addClass('next');
        $(this).prev().addClass('prev');
    }

    $(this).attr('id', 'slide-' + i);

    $('.navigations .dots').append(
        $('<li>', { class: i == 0 ? 'active' : '', id: i }).on('click', function() {
            var cSlide = $('.slider #slide-' + $(this).attr('id'));

            $('.navigations .dots li').removeClass('active');
            $(this).addClass('active');

            $('.slider .item').removeClass('active prev next');
            cSlide.addClass('active');
            cSlide.next().addClass('next');
            cSlide.prev().addClass('prev');
        })
    )
})