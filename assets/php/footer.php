<div class="reseau_sociaux">
    <h3>PLAY'ART</h3>
    <h4>Custom</h4>
    <div class="logo_rs">
        <img src="./assets/images/insta.svg" alt="Logo instagram">
        <img src="./assets/images/twitter.svg" alt="Logo twitter">
        <img src="./assets/images/facebook.svg" alt="Logo facebook">
        <img src="./assets/images/linkedin.svg" alt="Logo linkedin">
    </div>
</div>

<div class="footer_navigation">
    <div>
        <h3>Acheter</h3>
        <ul>
            <li>PS5</li>
            <li>Xbox serie X</li>
            <li>Nintendo Switch</li>
        </ul>
    </div>
    <div>
        <h3>Entreprise</h3>
        <ul>
            <li>Galerie</li>
            <li>Notre histoire</li>
            <li>Mentions légales</li>
        </ul>
    </div>
    <div>
        <h3>Assistance</h3>
        <ul>
            <li>Nous contacter</li>
            <li>Service clients</li>
        </ul>
    </div>
</div>

<div class="newsletter">
    <h3>Newsletter</h3>
    <p>Inscrivez-vous pour des promotions, des informations et plein d'autres trucs vraiment sympas !</p>
    <form action="" methode="POST">
        <input type="email" placeholder="Adresse mail">
        <button>INSCRIPTION</button>
    </form>
</div>