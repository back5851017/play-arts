<?php
require './assets/php/pdo.php';
session_start(); 
?>

<header>
    <img src="./assets/images/logo.svg" alt="Logo Play Art" id="logo" onclick='window.location.href="./index.php#presentation";'>
    <div class="navigation">
        <ul>
            <li><a href="./produit.php">Produits</a></li>
            <li><a href="./index.php#video">Notre histoire</a></li>
            <li><a href="./index.php#galerie">Galerie</a></li>
        </ul>
    </div>

    <div class="icone">
        <img src="./assets/images/panier.svg" alt="Icone panier">
        <?php
                if (isset($_SESSION['user'])) {
                    echo '<img src="./assets/images/logout.svg" alt="Icone connexion" onclick=`window.location.href="./logout.php";`>';
                } else {
                    echo '<img src="./assets/images/man.svg" alt="Icone connexion" onclick=`window.location.href="./login.php";`>';
                } 
        ?>
    </div>
</header>