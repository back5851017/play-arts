<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./assets/css/master.css?<?php echo rand(0,100);?>">
    <link rel="stylesheet" href="./assets/css/index.css?<?php echo rand(0,100);?>">
    <link rel="stylesheet" href="https://use.typekit.net/txw7aty.css">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css'>
    <title>Play'Art</title>
</head>

<body>


    <?php include './assets/php/header.php';  ?>
    
    <div class="scrollBar">
        <span></span>
    </div>

    <div class="container">
        <section class="v-slider-bloc" id="presentation">
            <div class="slogan">
                <h1 class="ml9">
                    <span class="text-wrapper">
                <span class="letters">Plus qu'une console, une oeuvre d'art</span>
                    </span>
                </h1>
                <h1 class="ml11">
                    <span class="text-wrapper">
                <span class="line line1"></span>
                    <span class="letters">Des designs uniques créés par une IA</span>
                    </span>
                </h1>
                <a href="produit.php">Nos produits</a>
            </div>
        </section>

        <section class="v-slider-bloc" id="galerie">

            <div class="slider">
                <div class="items-group">
                        <?php 

                            $query = $pdo->prepare("SELECT * FROM produit_categeroie Where  categorie != 'Mythologique' AND categorie !=   'Univers' AND categorie != 'Phoenix'");
                            $query->execute();
                            $products = $query->fetchAll();

                        ?>

                        <?php
                            for ($i = 0; $i < count($products); $i++) {
                        ?>
                    <div class="item">
                        <div class="blur" style="background-image: url('<?= $products[$i]["image_ps5"]; ?>'); clip-path: polygon(0% 0%, 40.3% 0%, 56.8% 100%, 0% 100%);"></div>
                        <div class="bg" style="background-image: url('<?= $products[$i]["image_ps5"]; ?>'); clip-path: polygon(0% 0%, 40.3% 0%, 56.8% 100%, 0% 100%);"></div>
                        <div class="blur2" style="background-image: url('<?= $products[$i]["image_xbox"]; ?>'); clip-path: polygon(100% 0%, 40.3% 0%, 56.8% 100%, 100% 100%);"></div>
                        <div class="bg2" style="background-image: url('<?= $products[$i]["image_xbox"]; ?>'); clip-path: polygon(100% 0%, 40.3% 0%, 56.8% 100%, 100% 100%);"></div>
                        <div class="block" style="background-image: url('<?= $products[$i]["image_categorie"]; ?>')">
                            <span class="circleLight"></span>
                            <div class="text">
                                <p><?= $products[$i]["nom"]; ?></p>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <div class="navigations">
                    <ul class="dots"></ul>
                </div>
            </div>
        </section>

        <section class="v-slider-bloc" id="video">
            <video autoplay muted loop id="timelaps">
                <source src="./assets/video/about.mp4" type="video/mp4">
            </video>
                <div class="inner-container">
                    <h1 class="fill-closing-horizontal-delayed">Notre Histoire</h1>
                    <p class="about-us">
                            Salut nous c’est Mathis, Guillaume, Maxime et Khenjin 4 potes qui ont eu pour ambition de faire des consoles de salons, un object design. Mais on avait aucune idée de comment réaliser ce projet alors on s’est posé et  une idée nous est apparu celle d’utiliser une intelligence artificielle pour créer des designs uniques, artistiques et abordables. Et c’est ainsi que Play’art est né, d'une simple idée de 4 potes.
                    </p>
                    <div class="competences">
                        <span>Custom</span>
                        <span>Design</span>
                        <span>Art</span>
                    </div>
                </div>
        </section>

        <section class="v-slider-bloc footer" id="footer">
            <?php include './assets/php/footer.php';  ?>
        </section>

    </div>


    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>

    <script src="./assets/js/scroll_timeline.js"></script>
    <script src="./assets/js/anime_text.js"></script>
    <script src="./assets/js/slider.js"></script>

</body>

</html>