<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./assets/css/master.css?<?php echo rand(0,100);?>">
    <link rel="stylesheet" href="./assets/css/register.css?<?php echo rand(0,100);?>">
    <link rel="stylesheet" href="./assets/css/login.css?<?php echo rand(0,100);?>">
    <link rel="stylesheet" href="./assets/css/alert.css?<?php echo rand(0,100);?>">

    <title>Play'Art</title>
</head>

<body>

    <?php include './assets/php/header.php';  ?>

    <div class="container">
        <section class="v-slider-bloc" id="register">
            <div class="global_container">
                <h2>Connexion</h2>
                <form class="form" method="post" action="login.php">
                    <div class="input-container">
                        <input type="text" name="email" required/>
                        <label for="email">Adresse mail</label>
                        <div class="bar"></div>
                    </div>
                    <div class="input-container">
                        <input type="password" name="password" required="required" />
                        <label for="password">Mot de passe</label>
                        <div class="bar"></div>
                    </div>
                    <a href="./register.php">Ou <span style="color: orange;">inscrivez-vous</span> ici !</a>
                    <div class="input-container">
                        <label class="check_box_container">Restez connecter
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                        <button>
                            Se connecter
                        </button>
                    </div>
                    <div class="alert_container">
                    <?php 
            
        session_start();

        if (isset($_SESSION['user'])) {
            header("Location: index.php");
        } else {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $login_email = $_POST['email'];
                $login_password = $_POST['password'];

                $error = false;
                if (!filter_var($login_email, FILTER_VALIDATE_EMAIL)) {
                    echo '<div class="alert danger-alert" role="alert">Erreur, l\'email est obligatoire !</div>';
                    $error = true;
                }

                if (empty($login_password)) {
                    echo '<div class="alert danger-alert" role="alert">Erreur, le mot de passe est obligatoire !</div>';
                    $error = true;
                }

                $query = $pdo->prepare("SELECT adresse_mail, nom FROM client WHERE adresse_mail = ? AND mot_de_passe = ?");
                $query->bindValue(1, $login_email);
                $query->bindValue(2, $login_password);
                $query->execute();
                $data = $query->fetch();

                if (empty($data)) {
                    echo '<div class="alert danger-alert" role="alert">Erreur, vos identifiants sont invalides !</div>';
                    $error = true;
                }

                if (!$error) {

                    $_SESSION['user'] = $data;
                    header("Location: index.php");
                    exit();
                }

            }
        }
            ?>
            </div>
            </div>
        </section>

        <section class="v-slider-bloc footer" id="footer">
            <?php include './assets/php/footer.php';  ?>
        </section>

    </div>
</body>

</html>