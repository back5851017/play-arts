-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 31 mars 2022 à 16:51
-- Version du serveur : 10.4.21-MariaDB
-- Version de PHP : 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `play'art`
--

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `adresse_mail` varchar(255) NOT NULL,
  `mot_de_passe` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(100) NOT NULL,
  `date_naissance` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`id`, `adresse_mail`, `mot_de_passe`, `nom`, `prenom`, `date_naissance`) VALUES
(3, 'lb.brangeon@gmail.com', 'mathislubin49', 'Mathis', '', '2022-03-30'),
(4, 'lb.brangeon@gmail.com', 'ezfffzfzf', 'Hey', '', '2022-03-30'),
(5, 'lb.brangeon@gmail.com', 'kezfjh', 'Mathis Brangeon', '', '2022-03-30'),
(6, 'test@gmail.com', 'zdazadza', 'efz', 'cae', '2003-09-10'),
(7, 'fzefez@gmail.com', 'ezfezf', 'ezfezfezeezff', 'ezfezfezf', '2010-09-10'),
(8, 'ef@effe.com', 'zddz', 'zdzdz', 'zdzdzdzdz', '2010-09-10'),
(9, 'ppefezpf@mgial.com', 'fezrf', 'j', 'bhv', '2003-09-10'),
(10, 'ppefezpf@mgial.com', 'fezrf', 'j', 'bhv', '2003-09-10');

-- --------------------------------------------------------

--
-- Structure de la table `produit_categeroie`
--

CREATE TABLE `produit_categeroie` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `nom_complet` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image_categorie` text NOT NULL,
  `image_ps5` text NOT NULL,
  `image_xbox` text NOT NULL,
  `categorie` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `produit_categeroie`
--

INSERT INTO `produit_categeroie` (`id`, `nom`, `nom_complet`, `description`, `image_categorie`, `image_ps5`, `image_xbox`, `categorie`) VALUES
(1, ' Megalopole', 'city', '', 'http://localhost/dev/assets/images/GALLERIE/carte_city.jpg', 'http://localhost/dev/assets/images/GALLERIE/ps4_city.png', 'http://localhost/dev/assets/images/GALLERIE/xbox_city.png', 'city'),
(2, 'Insomnie', 'night', '', 'http://localhost/dev/assets/images/GALLERIE/carte_night.jpg', 'http://localhost/dev/assets/images/GALLERIE/ps4_night.png', 'http://localhost/dev/assets/images/GALLERIE/xbox_night.png', 'night'),
(3, 'Landscape', 'paysage', '', 'http://localhost/dev/assets/images/GALLERIE/carte_paysage.jpg', 'http://localhost/dev/assets/images/GALLERIE/ps4_paysage.png', 'http://localhost/dev/assets/images/GALLERIE/xbox_paysage.png', 'paysage'),
(4, 'Ades', 'Ades', '', 'http://localhost/dev/assets/images/GALLERIE/Ades.jpg', 'http://localhost/dev/assets/images/GALLERIE/Hàdes_ps5.png', 'http://localhost/dev/assets/images/GALLERIE/Hadès_xbox.png', 'Mythologique'),
(5, 'Poséidon', 'Poséidon', '', 'http://localhost/dev/assets/images/GALLERIE/Poseidon.jpg', 'http://localhost/dev/assets/images/GALLERIE/Poseidon_ps5.png', 'http://localhost/dev/assets/images/GALLERIE/Poseidon_xbox.png', 'Mythologique'),
(6, 'Zeus', 'Zeus', '', 'http://localhost/dev/assets/images/GALLERIE/zeus.jpg', 'http://localhost/dev/assets/images/GALLERIE/Zeus_ps5.png', 'http://localhost/dev/assets/images/GALLERIE/Zeus_xbox.png', 'Mythologique'),
(7, 'Paix', 'Paix', '', 'http://localhost/dev/assets/images/GALLERIE/earth.jpg', 'http://localhost/dev/assets/images/GALLERIE/earth_ps5.png', 'http://localhost/dev/assets/images/GALLERIE/earth_xbox.png', 'Univers'),
(8, 'Imagination', 'Imagination', '', 'http://localhost/dev/assets/images/GALLERIE/univers.jpg', 'http://localhost/dev/assets/images/GALLERIE/univers_ps5.png', 'http://localhost/dev/assets/images/GALLERIE/univers_xbox.png', 'Univers'),
(9, 'Abysse', 'Abysse', '', 'http://localhost/dev/assets/images/GALLERIE/univers2.jpg', 'http://localhost/dev/assets/images/GALLERIE/univers_ps5.png', 'http://localhost/dev/assets/images/GALLERIE/univers2_xbox.png', 'Univers'),
(10, 'Pureté', 'Pureté', '', 'http://localhost/dev/assets/images/GALLERIE/blanc.jpg', 'http://localhost/dev/assets/images/GALLERIE/blanc_ps5.png', 'http://localhost/dev/assets/images/GALLERIE/blanc_xbox.png', 'Phoenix'),
(11, 'Rêve', 'Rêve', '', 'http://localhost/dev/assets/images/GALLERIE/violet.jpg', 'http://localhost/dev/assets/images/GALLERIE/violet_ps5.png', 'http://localhost/dev/assets/images/GALLERIE/violet_xbox.png', 'Phoenix'),
(12, 'Passion', 'Passion', '', 'http://localhost/dev/assets/images/GALLERIE/rouge.jpg', 'http://localhost/dev/assets/images/GALLERIE/rouge_ps5.png', 'http://localhost/dev/assets/images/GALLERIE/rouge_xbox.png', 'Phoenix');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `produit_categeroie`
--
ALTER TABLE `produit_categeroie`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `produit_categeroie`
--
ALTER TABLE `produit_categeroie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
