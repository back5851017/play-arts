<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./assets/css/master.css?<?php echo rand(0,100);?>">
    <link rel="stylesheet" href="./assets/css/produit.css?<?php echo rand(0,100);?>">

    <title>Play'Art</title>
</head>

<body>
    <?php include './assets/php/header.php';  ?>
    
        <?php
                $categorie = ['Univers', 'Phoenix', 'Mythologique'];
        ?>

        <div class="entente_produit">
            <h2>NOS PRODUITS</h2>
            <h4>Nos collections sont en <span style="color: orange;">stock limités</span></h4>
        </div>
                   <?php for ($i=0; $i < count($categorie); $i++) { 
                                    $query = $pdo->prepare("SELECT * FROM produit_categeroie Where categorie = '$categorie[$i]'");
                                    $query->execute();
                                    $products = $query->fetchAll();
                    ?> 
                                <div class="categorie_container">
                                        <h3><?= $categorie[$i]; ?></h3>
                                            <div class="image_categorie_container">
                                                    <?php for ($y=0; $y < count($products) ; $y++) { ?>
                                                        <a href='produit_id.php?id=<?=$products[$y]["id"]?>' class="image" style="background-image: url('<?= $products[$y]["image_categorie"]; ?>')">
                                                            <h3><?= $products[$y]["nom"]; ?></h3>
                                                    </a>  
                                                    <?php } ?>
                                            </div>
                                </div>
                <?php } ?>
    <section class="v-slider-bloc footer" id="footer">
        <?php include './assets/php/footer.php';  ?>
    </section>

</body>

</html>