<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./assets/css/master.css?<?php echo rand(0,100);?>">
    <link rel="stylesheet" href="./assets/css/produit_id.css?<?php echo rand(0,100);?>">
    <title>Play'Art</title>
</head>

<body>
    <?php include './assets/php/header.php';  ?>
    
    <div class="container">
        <section class="v-slider-bloc" id="produit">
            <?php 
    
            $id_product =  $_GET['id'];
            $query = $pdo->prepare("SELECT * FROM produit_categeroie where id = $id_product");
            $query->execute();
            $products = $query->fetch();
            
            ?>

            <div class="container_slide">
                <div class="mySlides fade">        
                    <img src="<?=$products['image_ps5'] ?>" class="background ps5">
                </div>
                <div class="mySlides fade">        
                    <img src="<?=$products['image_xbox'] ?>" class="background">
                </div>
                <div class="container-dot">
                    <span class="dot" onclick="currentSlide(1)"></span>
                    <span class="dot" onclick="currentSlide(2)"></span>
                </div>
            </div>

            <div class="container_information">
                <h2 class="title"><?=$products['nom_complet'] ?></h2>
                <p class="desc"><?=$products['description'] ?></p>
                <div class="buy">
                    <hr>
                    <div class="type">
                        <p>Console</p>
                        <select name="console" class="minimal">
                            <option value="PS5">PS5</option>
                            <option value="Xbox_one_serie_X">Xbox One serie X</option>
                        </select>
                    </div>
                    <div class="type">
                        <p>Options</p>
                        <select name="options" class="minimal">
                            <option value="1_manette">1 Manette</option>
                            <option value="2_manette">2 Manettes</option>
                        </select>
                    </div>
                    <hr>
                    <div class="bottom">
                        <div class="quantite">
                            <h2>Quantite</h2>
                            <input type="number">
                        </div>
                        <button>Ajouter au panier</button>
                    </div>
                </div>
            </div>

        </section>

        <section class="v-slider-bloc footer" id="footer">
            <?php include './assets/php/footer.php';  ?>
        </section>

    </div>

    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js'></script>
    <script src="./assets/js/image_slider.js"></script>
    <script src="./assets/js/select.js"></script>
</body>

</html>