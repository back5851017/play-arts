<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./assets/css/master.css?<?php echo rand(0,100);?>">
    <link rel="stylesheet" href="./assets/css/register.css?<?php echo rand(0,100);?>">
    <link rel="stylesheet" href="./assets/css/alert.css?<?php echo rand(0,100);?>">


    <title>Play'Art</title>
</head>

<body>


    <?php include './assets/php/header.php';  ?>

    <div class="container">
        <section class="v-slider-bloc" id="register">
            <div class="global_container">
                <h2>Inscription</h2>
                <form class="form" method="post">
                    <div class="input-container">
                        <input type="email" name="email"  required/>
                        <label for="email">Adresse mail</label>
                        <div class="bar"></div>
                    </div>
                    <div class="input-container">
                        <input type="password" name="mdp"  required/>
                        <label for="mdp">Mot de passe</label>
                        <div class="bar"></div>
                    </div>
                    <div class="input-container">
                        <input type="text" name="name" required />
                        <label for="name">Nom</label>
                        <div class="bar"></div>
                    </div>
                    <div class="input-container">
                        <input type="text" name="nom"  required/>
                        <label for="nom">Prenom</label>
                        <div class="bar"></div>
                    </div>
                    <div class="input-container">
                        <input type="date" name="date"  required/>
                        <label for="date">Date de naissance</label>
                        <div class="bar"></div>
                    </div>
                    <div class="input-container">
                        <label class="check_box_container">Inscrivez-vous pour recevoir par e-mail les dernières infos sur les produits et les nouvelles collection de Play’art.
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                        <button>
                            S'inscrire
                        </button>
                    </div>

                    <div class="alert_container">
                        <?php      
        require "./assets/php/pdo.php";
        session_start();

        if (isset($_SESSION['user'])) {
            header("Location: index.php");
        } else {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $register_name = $_POST['name'];
                $register_email = $_POST['email'];
                $register_password = $_POST['mdp'];
                $register_nom = $_POST['nom'];
                $register_date_naissance = $_POST['date'];

                $error = false;

                if (empty($register_name)) {
                    echo '<div class="alert danger-alert" role="alert">Erreur, le prénom est requis !</div>';
                    $error = true;
                }

                if (!filter_var($register_email, FILTER_VALIDATE_EMAIL)) {
                    echo '<div class="alert danger-alert" role="alert">Erreur, l\'email est obligatoire !</div>';
                    $error = true;
                }

                if (empty($register_password)) {
                    echo '<div class="alert danger-alert" role="alert">Erreur, le mot de passe est obligatoire !</div>';
                    $error = true;
                }

                if (empty($register_nom)) {
                    echo '<div class="alert danger-alert" role="alert">Erreur, le nom est obligatoire !</div>';
                    $error = true;
                }

                 if (empty($register_date_naissance)) {
                    echo '<div class="alert danger-alert" role="alert">Erreur, la date de naissance est obligatoire !</div>';
                    $error = true;
                }

                if (!$error) {

                    $query = $pdo->prepare("INSERT INTO client(adresse_mail, mot_de_passe, nom, prenom, date_naissance) VALUES(?,?,?,?,?)");
                    $query->bindValue(1, $register_email);
                    $query->bindValue(2, $register_password);
                    $query->bindValue(3, $register_name);
                    $query->bindValue(4, $register_nom);
                    $query->bindValue(5, $register_date_naissance);
                    $query->execute();

                    header("Location: login.php");
                    exit();
                }
            }
        }
        ?>
                    </div>
            </div>
        </section>

        <section class="v-slider-bloc footer" id="footer">
            <?php include './assets/php/footer.php';  ?>
        </section>

    </div>
</body>

</html>